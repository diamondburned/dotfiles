# Diamond's dotfiles

Here are its dotfiles. They're managed by Nix. Simply clone this repository,
set up a symlink wiring to `./Scripts/nix/configuration.nix` and it's good.

## Quick Links

- **lineprompt**: [Scripts/lineprompt](Scripts/lineprompt)
- **Nix dotfiles**: [Scripts/nix/configuration.nix](Scripts/nix/configuration.nix)
- **All configurations**: [Scripts/nix/cfg](Scripts/nix/cfg)
- **Neovim configuration**: [Scripts/nix/cfg/nvim](Scripts/nix/cfg/nvim)
