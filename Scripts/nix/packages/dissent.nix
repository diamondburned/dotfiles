{
  pkgs,
  lib,
  _inputs,
}:

let
  dissent = {
    version = "0.0.15";
    src = _inputs.dissent;
    base = import "${dissent.src}/nix/base.nix" {
      inherit pkgs;
      inherit (dissent) src;
    };
  };

  gotk4-nix = pkgs.fetchFromGitHub {
    owner = "diamondburned";
    repo = "gotk4-nix";
    rev = "4f498cd56a726dc2ecb19af471cb43bb759708bb";
    hash = "sha256:0009jbdj2y2vqi522a3r64xf4drp44ghbidf32j6bslswqf3wy4m";
  };

  libspelling_2_1 = pkgs.libspelling.overrideAttrs {
    version = "0.2.1";

    src = pkgs.fetchFromGitLab {
      domain = "gitlab.gnome.org";
      owner = "GNOME";
      repo = "libspelling";
      rev = "refs/tags/0.2.1";
      hash = "sha256-0OGcwPGWtYYf0XmvzXEaQgebBOW/6JWcDuF4MlQjCZQ=";
    };
  };
in

pkgs.stdenv.mkDerivation {
  pname = dissent.base.pname;
  inherit (dissent) version src;

  # buildInputs =
  #   (dissent.base.buildInputs or (_: [ ])) pkgs
  #   ++ (with pkgs; [
  #     gtk4
  #     glib
  #     librsvg
  #     gdk-pixbuf
  #     gobject-introspection
  #     hicolor-icon-theme
  #     libspelling_2_1
  #     gtksourceview5
  #   ]);
  #
  # nativeBuildInputs =
  #   (dissent.base.nativeBuildInputs or (_: [ ])) pkgs
  #   ++ (with pkgs; [
  #     wrapGAppsHook
  #     autoPatchelfHook
  #   ]);

  sourceRoot = ".";

  buildPhase = with dissent.base; ''
    mkdir -p \
    	$out/share/dbus-1/services \
    	$out/share/applications \
    	$out/share/icons
    install -Dm644 ${src}/nix/so.libdb.dissent.service $out/share/dbus-1/services/so.libdb.dissent.service
    install -Dm644 ${files.desktop.path} $out/share/applications/${files.desktop.name}
    cp -r --no-preserve=mode,ownership ${files.icons.path}/* $out/share/icons/
  '';
}
