require("todo-comments").setup {
	signs = true,
	keywords = {
		FIX  = { icon = "" },
		TODO = { icon = "" },
		HACK = { icon = "" },
		WARN = { icon = "" },
		PERF = { icon = "" },
		NOTE = { icon = "" },
		TEST = { icon = "" },
	},
}
